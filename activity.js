db.users.insertMany([
	{
		"fName":"Mandy",
		"lName":"Maker",
		"email":"Mandy102@tech.com",
		"password":"102ggs",
		"isAdmin":false
	},
	{
		"fName":"Greydi",
		"lName":"Joe",
		"email":"Greyjo@gmail.com",
		"password":"qwerty12",
		"isAdmin":false
	},
	{
		"fName":"Boom",
		"lName":"Boomer",
		"email":"Booomski@yahoo.com",
		"password":"sabog53",
		"isAdmin":false
	}
])

db.prods.insertMany([
	{
		"id": 1,
		"name": "Razer Black Widow",
		"desc": "Gaming keyboard",
		"price": 1300,
		"isActive": true,
	},
	{
		"id": 2,
		"name": "Razer Viper Mini",
		"desc": "Gaming mouse",
		"price": 8000,
		"isActive": true,
	},
	{
		"id": 3,
		"name": "Razer Black Shark V2X",
		"desc": "Gaming headset",
		"price": 3000,
		"isActive": true,
	}
])

db.users.find({$or:[{fName:{$regex:'a',$options:'$i'}},{lName:{$regex:'a',$options:'$i'}}]},{email:1,isAdmin:1})

db.users.find({$and:[{fName:{$regex:'i',$options:'$i'}},{isAdmin:true}]},{email:1,isAdmin:1})

db.users.deleteMany({email:{$regex:'tech',$options:'$i'}})

db.prods.find({name:{$regex:'x',$options:'$i'},price:{$gte:50000}})

db.prods.updateMany({price:{$lt:1000}},{$set:{isActive:false}})